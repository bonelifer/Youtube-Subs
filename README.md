# Youtube-Subs
Use youtube-dl and mpv to watch your favorite Youtube channels without a browser

#Strip down Youtube Sub page HTML for usable list
grep "yt-simple-endpoint" /tmp/yt.tmp|grep href|cut -d\" -f6|grep -e "user" -e "channel"|sort -u|while read line;do echo "https://www.youtube.com$line/videos";done
